<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserDetailsRepository")
 */
class UserDetails
{

    const SEXE_MALE = "MALE";
    const SEXE_FEMALE = "FEMALE";


    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebook_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $twitter_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $pinterest_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $website_url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sexe = self::SEXE_FEMALE;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $marital_status;

    /**
     * @ORM\Column(type="smallint")
     */
    private $number_children = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $short_description;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $favorite_dish;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacebookUrl(): ?string
    {
        return $this->facebook_url;
    }

    public function setFacebookUrl(?string $facebook_url): self
    {
        $this->facebook_url = $facebook_url;

        return $this;
    }

    public function getTwitterUrl(): ?string
    {
        return $this->twitter_url;
    }

    public function setTwitterUrl(?string $twitter_url): self
    {
        $this->twitter_url = $twitter_url;

        return $this;
    }

    public function getPinterestUrl(): ?string
    {
        return $this->pinterest_url;
    }

    public function setPinterestUrl(?string $pinterest_url): self
    {
        $this->pinterest_url = $pinterest_url;

        return $this;
    }

    public function getWebsiteUrl(): ?string
    {
        return $this->website_url;
    }

    public function setWebsiteUrl(?string $website_url): self
    {
        $this->website_url = $website_url;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getSexe(): ?string
    {
        return $this->sexe;
    }

    public function setSexe(string $sexe): self
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function getMaritalStatus(): ?string
    {
        return $this->marital_status;
    }

    public function setMaritalStatus(string $marital_status): self
    {
        $this->marital_status = $marital_status;

        return $this;
    }

    public function getNumberChildren(): ?int
    {
        return $this->number_children;
    }

    public function setNumberChildren(int $number_children): self
    {
        $this->number_children = $number_children;

        return $this;
    }

    public function getShortDescription(): ?string
    {
        return $this->short_description;
    }

    public function setShortDescription(?string $short_description): self
    {
        $this->short_description = $short_description;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getFavoriteDish(): ?string
    {
        return $this->favorite_dish;
    }

    public function setFavoriteDish(?string $favorite_dish): self
    {
        $this->favorite_dish = $favorite_dish;

        return $this;
    }
}
